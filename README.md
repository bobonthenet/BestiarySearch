# Pathfinder Bestiary search
1. clone this repo
2. Install Python dependencies.
  ```
  pip install -r requirements.txt
  ```
  Note that it would probably better to not install these globally but I'm lazy.
3. In the root director npm install.
4. You'll also need to cd to the webapp dir and npm install there too.
  * Sorry it takes forever, grab a beer.
5. In the root directory use the following command.
  ```npm run start```
6. If you're browser doesn't open a new tab with the bestiary search or the search doesn't return results then please send an email to zafthan@gmail.com and let me know what issue you are seeing.
