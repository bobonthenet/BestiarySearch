https://flask-restless.readthedocs.io/en/stable/searchformat.html

# Examples
http://127.0.0.1:5000/bestiary/monster?q={"filters":[{"name":"name","op":"like","val":"goblin%"}],"single":false}
Not a smart query but good for seeing how multiple filters would work http://127.0.0.1:5000/api/monster?q={filters:[{name:name,op:like,val:goblin%},%20{name:name,op:like,val:%Dog}],single:false}

http://127.0.0.1:5000/bestiary/monster?q={"filters":[{"name":"ecology__environment","op":"any","val":"%temperate%"}],"single":false}

http://127.0.0.1:5000/bestiary/monster?q={"filters":[{"name":"ecology","op":"any","val":{"name":"environment","op":"like","val":"%temperate%"}}],"single":false,"order_by":[{"field":"name","direction":"asc"}]}
