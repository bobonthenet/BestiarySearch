import flask
import flask_sqlalchemy
import flask_restless
from flask_cors import CORS

app = flask.Flask(__name__)
CORS(app)
app.config.update(
    DEBUG=True,
    SQLALCHEMY_DATABASE_URI='sqlite:///bestiary.db',
    SQLALCHEMY_TRACK_MODIFICATIONS=False
)
db = flask_sqlalchemy.SQLAlchemy(app)

class Monster(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    description_visual = (db.Text)
    cr = db.Column(db.Integer)
    xp = db.Column(db.Integer)
    race = db.Column(db.Text)
    mclass = db.Column(db.Text)
    alignment = db.Column(db.Text)
    size = db.Column(db.Text)
    mtype = db.Column(db.Text)
    sub_type = db.Column(db.Text)
    init = db.Column(db.Integer)
    senses = db.Column(db.Text)
    aura = db.Column(db.Text)
    defense = db.relationship('Defense', backref=db.backref('monster'))
    ecology = db.relationship('Ecology', backref=db.backref('monster'))
    offense = db.relationship('Offense', backref=db.backref('monster'))
    special_abilities = db.relationship('Special_Abilities', backref=db.backref('monster'))
    statistics = db.relationship('Statistics', backref=db.backref('monster'))


class Defense(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    monster_id = db.Column(db.Integer, db.ForeignKey('monster.id'))
    ac = db.Column(db.Text)
    ac_mods = db.Column(db.Text)
    hp = db.Column(db.Text)
    hd = db.Column(db.Integer)
    fort = db.Column(db.Integer)
    ref = db.Column(db.Integer)
    will = db.Column(db.Integer)
    damage_reduction = db.Column(db.Text)
    immune = db.Column(db.Text)
    resist = db.Column(db.Text)
    spell_resistance = db.Column(db.Integer)
    weaknesses = db.Column(db.Text)

class Ecology(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    monster_id = db.Column(db.Integer, db.ForeignKey('monster.id'))
    environment = db.Column(db.Text)
    organization = db.Column(db.Text)
    treasure = db.Column(db.Text)
    description = db.Column(db.Text)

class Offense(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    monster_id = db.Column(db.Integer, db.ForeignKey('monster.id'))
    speed = db.Column(db.Text)
    speed_mod = db.Column(db.Text)
    melee = db.Column(db.Text)
    ranged = db.Column(db.Text)
    space = db.Column(db.Text)
    reach = db.Column(db.Text)
    special_attacks = db.Column(db.Text)
    spell_like_abilities = db.Column(db.Text)
    spells_known = db.Column(db.Text)

class Special_Abilities(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    monster_id = db.Column(db.Integer, db.ForeignKey('monster.id'))
    special_abilities = db.Column(db.Text)

class Statistics(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    monster_id = db.Column(db.Integer, db.ForeignKey('monster.id'))
    ability_scores = db.Column(db.Text)
    ability_score_mods = db.Column(db.Text)
    base_attack = db.Column(db.Integer)
    cmb = db.Column(db.Integer)
    cmd = db.Column(db.Integer)
    feats = db.Column(db.Text)
    skills = db.Column(db.Text)
    racial_mods = db.Column(db.Text)
    languages = db.Column(db.Text)
    special_qualities = db.Column(db.Text)

db.create_all()

manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)
methods = ['GET']
url_prefix = '/bestiary'
exclude_columns=['id', 'moster_id']
manager.create_api(Monster, methods=methods, url_prefix=url_prefix, results_per_page=20)
manager.create_api(Defense, methods=methods, url_prefix=url_prefix, exclude_columns=exclude_columns)
manager.create_api(Defense, methods=methods, url_prefix=url_prefix, exclude_columns=exclude_columns)
manager.create_api(Ecology, methods=methods, url_prefix=url_prefix, exclude_columns=exclude_columns)
manager.create_api(Offense, methods=methods, url_prefix=url_prefix, exclude_columns=exclude_columns)
manager.create_api(Special_Abilities, methods=methods, url_prefix=url_prefix, exclude_columns=exclude_columns)
manager.create_api(Statistics, methods=methods, url_prefix=url_prefix, exclude_columns=exclude_columns)

app.run(host='0.0.0.0')
