#!/bin/sh

cd webapp && npm install && cd ..

docker build -t bobonthenet/monsterapi api
docker run -d -p 5000:5000 bobonthenet/monsterapi

docker build -t bobonthenet/monsterwebapp webapp
docker run -d -p 80:80 bobonthenet/monsterwebapp
