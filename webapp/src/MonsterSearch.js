import React from "react";
import Client from "./Client";

const MATCHING_ITEM_LIMIT = 25;

class MonsterSearch extends React.Component {
  state = {
    monsters: [],
    showRemoveIcon: false,
    searchValue: ""
  };

  handleSearchChange = e => {
    const value = e.target.value;

    this.setState({
      searchValue: value
    });

    if (value === "") {
      this.setState({
        monsters: [],
        showRemoveIcon: false
      });
    } else {
      this.setState({
        showRemoveIcon: true
      });

      Client.search(value, monsters => {
        this.setState({
          monsters: monsters.objects.slice(0, MATCHING_ITEM_LIMIT)
        });
      });
    }
  };

  handleSearchCancel = () => {
    this.setState({
      monsters: [],
      showRemoveIcon: false,
      searchValue: ""
    });
  };

  render() {
    const { showRemoveIcon, monsters } = this.state;
    const removeIconStyle = showRemoveIcon ? {} : { visibility: "hidden" };

    const monsterRows = monsters.map((monster, idx) => (
      <tr key={idx} onClick={() => this.props.onMonsterClick(monster)}>
        <td>{monster.name}</td>
        <td className="right aligned">{monster.cr}</td>
        <td className="right aligned">{monster.xp}</td>
        <td className="right aligned">{monster.ecology[0].environment}</td>
        {/* <td className="right aligned">{monster.size}</td>
        <td className="right aligned">{monster.init}</td> */}
      </tr>
    ));

    return (
      <div id="monster-search">
        <table className="ui selectable structured large table">
          <thead>
            <tr>
              <th colSpan="5">
                <div className="ui fluid search">
                  <div className="ui icon input">
                    <input
                      className="prompt"
                      type="text"
                      placeholder="Search monsters..."
                      value={this.state.searchValue}
                      onChange={this.handleSearchChange}
                    />
                    <i className="search icon" />
                  </div>
                  <i
                    className="remove icon"
                    onClick={this.handleSearchCancel}
                    style={removeIconStyle}
                  />
                </div>
              </th>
            </tr>
            <tr>
              <th className="eight wide">Name</th>
              <th>cr</th>
              <th>xp</th>
              <th>environment</th>
            </tr>
          </thead>
          <tbody>
            {monsterRows}
          </tbody>
        </table>
      </div>
    );
  }
}

export default MonsterSearch;
