import React, { Component } from "react";
import MonsterSearch from "./MonsterSearch";

class App extends Component {
  state = {
    selectedMonsters: []
  };

  removeMonsterItem = itemIndex => {
    const filteredMonsters = this.state.selectedMonsters.filter(
      (item, idx) => itemIndex !== idx
    );
    this.setState({ selectedMonsters: filteredMonsters });
  };

  addMonster = monster => {
    const newMonsters = this.state.selectedMonsters.concat(monster);
    this.setState({ selectedMonsters: newMonsters });
  };

  render() {
    return (
      <div className="App">
        <div className="ui text container">
          <MonsterSearch onMonsterClick={this.addMonster} />
        </div>
      </div>
    );
  }
}

export default App;
